FROM amd64/node:8.9.1-alpine as builder

RUN apk add --no-cache git
WORKDIR /src
RUN git clone https://github.com/burkl/maxcube.git && \
    npm install ./maxcube && \
    npm install mqtt --save

#
# -------
#
FROM amd64/node:8.9.1-alpine

COPY --from=builder /usr/local/bin/node /usr/local/bin/node
COPY --from=builder /usr/local/lib/node_modules /usr/local/lib/node_modules
COPY --from=builder /src /src

COPY ./maxcube.js /src/maxcube.js

WORKDIR /src
CMD node maxcube.js

#--------------------------------------------------------------------------------
# Labelling
#--------------------------------------------------------------------------------

ARG BUILD_DATE
ARG VCS_REF
ARG VCS_URL
ARG VERSION

LABEL de.5square.homesmarthome.build-date=$BUILD_DATE \
      de.5square.homesmarthome.name="homesmarthome/maxcube" \
      de.5square.homesmarthome.description="MQTT message when Window Switch chnage is detected from MaxCube LAN Gateway" \
      de.5square.homesmarthome.url="homesmarthome.5square.de" \
      de.5square.homesmarthome.vcs-ref=$VCS_REF \
      de.5square.homesmarthome.vcs-url="$VCS_URL" \
      de.5square.homesmarthome.vendor="5square" \
      de.5square.homesmarthome.version=$VERSION \
      de.5square.homesmarthome.schema-version="1.0"
