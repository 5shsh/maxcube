var MaxCube = require('./maxcube');
var myMaxCube = new MaxCube(process.env.MAXCUBE_HOST, 62910);

var mqtt = require('mqtt')
var client  = mqtt.connect('mqtt://' + process.env.MQTT_HOST)

var topic = '/maxcube/sensor/';

var prevStatus = [];

function observe(devices) {

  devices.forEach(function (device, i) {
    var addr = device.rf_address;
    var deviceInfo = myMaxCube.getDeviceInfo(addr);

    if (prevStatus[i] != device.open) {
      console.log(addr, 'Status changed to', device.open);
      var t = topic + deviceInfo.room_name + '.' + deviceInfo.device_name.toLowerCase();
      client.publish(t, JSON.stringify({"open": device.open, "addr": addr}));
    }

    prevStatus[i] = device.open;
  });

  setTimeout(function() {
    myMaxCube.flushDeviceCache();
    myMaxCube.getDeviceStatus().then(function (devices) {
      observe(devices);
    });
  }, 5000);
}

client.on('connect', function () {
  console.log('Connected to MQTT');

  myMaxCube.on('connected', function () {
    console.log('Connected to MaxCube');

    myMaxCube.getDeviceStatus().then(function (devices) {
      observe(devices);
    });

  });
});

myMaxCube.on('closed', function () {
  console.log('Connection closed');
});
