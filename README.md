# MaxCube

[![](https://images.microbadger.com/badges/image/homesmarthome/maxcube.svg)](https://microbadger.com/images/homesmarthome/maxcube "Get your own image badge on microbadger.com")

[![](https://images.microbadger.com/badges/version/homesmarthome/maxcube.svg)](https://microbadger.com/images/homesmarthome/maxcube "Get your own version badge on microbadger.com")

Sends an MQTT message if a Maxcube windows sensor detects a status change.

## Run
```
docker run -d \
  --restart=unless-stopped \
  --name=maxcube \
  -e MQTT_HOST=10.1.2.3 \
  -e MAXCUBE_HOST==10.1.2.3.4 \
  homesmarthome/maxcube::latest
```

## Swarm
```
docker service create \
  --name maxcube \
  -e MQTT_HOST=10.1.2.3 \
  -e MAXCUBE_HOST==10.1.2.3.4 \
  homesmarthome/maxcube::latest
```